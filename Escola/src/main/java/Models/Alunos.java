package Models;

// TODO: Auto-generated Javadoc
/**
 * The Class Alunos.
 */
public class Alunos {
	
	/** The idaluno. */
	private String idaluno;
	
	/** The nome. */
	private String nome;
	
	/** The idade. */
	private String idade;
	
	/** The idturma. */
	private String idturma;
	
	
	
	
	/**
	 * Instantiates a new alunos.
	 */
	public Alunos() {
		super();
		
	}
	
	/**
	 * Instantiates a new alunos.
	 *
	 * @param idaluno the idaluno
	 * @param nome the nome
	 * @param idade the idade
	 * @param idturma the idturma
	 */
	public Alunos(String idaluno, String nome, String idade, String idturma) {
		super();
		this.idaluno = idaluno;
		this.nome = nome;
		this.idade = idade;
		this.idturma = idturma;
	}

	/**
	 * Gets the idaluno.
	 *
	 * @return the idaluno
	 */
	public String getIdaluno() {
		return idaluno;
	}
	
	/**
	 * Sets the idaluno.
	 *
	 * @param idaluno the new idaluno
	 */
	public void setIdaluno(String idaluno) {
		this.idaluno = idaluno;
	}
	
	/**
	 * Gets the nome.
	 *
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Sets the nome.
	 *
	 * @param nome the new nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Gets the idade.
	 *
	 * @return the idade
	 */
	public String getIdade() {
		return idade;
	}
	
	/**
	 * Sets the idade.
	 *
	 * @param idade the new idade
	 */
	public void setIdade(String idade) {
		this.idade = idade;
	}
	
	/**
	 * Gets the idturma.
	 *
	 * @return the idturma
	 */
	public String getIdturma() {
		return idturma;
	}
	
	/**
	 * Sets the idturma.
	 *
	 * @param idturma the new idturma
	 */
	public void setIdturma(String idturma) {
		this.idturma = idturma;
	}

	
}
