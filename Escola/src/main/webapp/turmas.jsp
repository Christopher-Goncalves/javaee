<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="Models.Turma"%>
<%@ page import="java.util.ArrayList"%>
<%
@SuppressWarnings("unchecked")
ArrayList<Turma> lista = (ArrayList<Turma>) request.getAttribute("turma");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Gerenciamento de Alunos</title>
<link rel="Icon" href="Imagens/regua.png">
<link rel="stylesheet" href="Style/style.css">
</head>
<body>
	<div class="principal">
		<img alt="" src="Imagens/lista.png">
		<h1>Lista de turmas</h1>
		<a href="novoAluno.html" class="Botao3">cadastrar aluno</a> <a
			href="Index.html" class="voltar">Voltar</a>
		<table id="tabela">
			<thead>
				<tr>
					<th>TURMA</th>
					<th>ID</th>
				</tr>
			</thead>
			<tbody>
				<%
				for (int i = 0; i < lista.size(); i++) {
				%>
				<tr>
					<td><%=lista.get(i).getNome()%></td>
					<td><%=lista.get(i).getIdturma()%></td>
					<td><a href="alunos?idturma=<%=lista.get(i).getIdturma()%>"
						class="Botao">Ver Alunos</a></td>
				</tr>
				<%
				}
				%>
			</tbody>
		</table>
	</div>
</body>
</html>
