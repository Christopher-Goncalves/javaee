<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ page import="Models.Alunos"%>
<%@ page import="java.util.ArrayList"%>
<%
@SuppressWarnings("unchecked")
ArrayList<Alunos> lista = (ArrayList<Alunos>) request.getAttribute("alunos");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Gerenciamento de Alunos</title>
<link rel="Icon" href="Imagens/regua.png">
<link rel="stylesheet" href="Style/alunos.css">
</head>
<body>
<div class="principal">
<img alt="" src="Imagens/lista.png">
	<h1>Lista de turmas</h1>
	<table id="tabela">
		<thead>
			<tr>
				<th>Alunos</th>
				<th>Idade</th>
				<a href="main" class="voltar">voltar</a>
			</tr>
		</thead>
		<tbody>
			<%
			for (int i = 0; i < lista.size(); i++) {
			%>
			<tr>
				<td><%=lista.get(i).getNome()%></td>
				<td><%=lista.get(i).getIdade()%></td>
				<td><a href="select?idaluno=<%=lista.get(i).getIdaluno()%>"
					class="Botao">editar</a></td>
					<td><a href="javascript:confirmar(<%=lista.get(i).getIdaluno()%>)"
					class="Botao2">Excluir</a></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
		<script src="Scripts/confirmar.js"></script>
		</div>
</body>
</html>