<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Gerenciamento de Alunos</title>
<link rel="Icon" href="Imagens/regua.png">
<link rel="stylesheet" href="Style/editar.css">
</head>
<body>
<div class="principal">
	<h1>editar</h1>
	<form action="update" name="formAluno">
		<table id="tabela">
			<tr>
				<td><label>Id Aluno<input type="text" name="idaluno" class="Caixa1" readonly
					value="<%out.print(request.getAttribute("idaluno"));%>"></label></td>
			</tr>
			<tr>
				<td></label> Nome<input type="text" name="nome" class="Caixa1"
					value="<%out.print(request.getAttribute("nome"));%>"></label></td>
			</tr>
			<tr>
				<td></label> Idade<input type="text" name="idade" class="Caixa1"
					value="<%out.print(request.getAttribute("idade"));%>"></label></td>
			</tr>
			<tr>
				<td></label>Id Turma<input type="text" name="idturma" class="Caixa1"
					value="<%out.print(request.getAttribute("idturma"));%>"></label></td>
			</tr>
		</table>
		<input type="button" value="Salvar" class="Botao"
			onclick="Validar()">
	</form>
	<script src="Scripts/validador.js"></script>
	</div>
</body>
</html>